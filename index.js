// setup the depedencies
const express = require("express");
const mongoose = require("mongoose");

// import the taskRoutes
 const taskRoute = require("./routes/taskRoute");



// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// connecting to the moongodb atlas
mongoose.connect("mongodb+srv://dbnovengorospe:efBaWxyisxGMqFY8@wdc028-course-booking.68ll3.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology : true
	}

);

// Add task route
app.use("/tasks", taskRoute)
// localhost:3001/tasks/getall - get all is being get in taskroute


// server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));