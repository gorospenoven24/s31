// setup express dependency
const express = require("express");
// create a router instance
const router = express.Router();
// iport TaskController.js
const taskController = require("../controllers/taskController");

// Routes

// Routes to get all task
router.get("/", (req,res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Rote to create a new task
router.post("/", (req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})



//  Route for deleting a task
//localhost:3001/tasks/3313113131
router.delete("/:id", (req, res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route for updating  a task 
router.put("/:id", (req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// ///////////////////---------------Activity-----------------------///////////////////////////////////////////

// Routes to get aspeccfic task task
router.get("/:id", (req,res)=>{
	taskController.getSpecificTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Route for updating  a task 
router.put("/:id/complete", (req,res)=>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router