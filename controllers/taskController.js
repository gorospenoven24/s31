// importing the model
const Task = require("../models/task");

// Controller function for getting all the task
module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result =>{
		return result;
	})
}

// Contorller function for createTask
module.exports.createTask = (requestBody) =>{

	// create a task object based on the mongodb model "Task"
	let newTask =  new Task({
		// Sets the "name" property with the value receive from the client /postman
		name:requestBody.name
	})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})

}

// controller function for deleteTask
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// controller fumction for updating a task
module.exports.updateTask = (taskId, newContent)=>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}


// Controller function for getting specific  task
module.exports.getSpecificTasks = (taskId)=>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return result;
		}
	})
}

// controller fumction for updating a task
module.exports.updateStatus = (taskId, newStatus)=>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false;
		}
	 result.status = newStatus.status;
		return result.save().then((updatedStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedStatus;
			}
		})
	})
}
